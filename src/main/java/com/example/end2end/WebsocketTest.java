package com.example.end2end;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.WebSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class WebsocketTest {
    public static void main(String[] args) {
        var httpClient = HttpClient.newHttpClient();
        var wsFut = httpClient.newWebSocketBuilder()
                .header("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJ1aWQiOjEsInN0cmVhbWVyIjpmYWxzZSwiamlkIjoiMUBkcmFnb24tZGV2LmZpcmVjbG91ZC5saXZlIiwiZ2VuZGVyIjoiVU5ERUZJTkVEIiwibGV2ZWwiOjAsIm5pY2tuYW1lIjoiRMawxqFuZyBUaeG6v24gxJDhuqF0IiwidmlwIjowLCJleHAiOjE2MjM4NjkwOTAsImlhdCI6MTYyMDI2OTA5MCwiYWNjb3VudF9ubyI6IjUwMjUzODAwIiwianRpIjoiNGI2MWJkYTYtYjhkNy00NjUxLWExYmQtYTZiOGQ3MTY1MWJiIn0.tJITsjILLVOPsRkogzPi6_peopXzA3KSVRRwFWbua4QnVgzvCTIqG_0Rdi5_pbnWNYDydBI0-SiRzNaaaU_soFWwQkoIcTqz4YYnjIw3k0oQsh3zAQ-fJ-f_8OV6PQGZbKS-XbznLtHbgVfQ1KK4N2Jv4URH4_F6az_18ZqDfDVMR7WaOzx78vZB_JAnXC4F88hcFCQd6Gpn9hfUL6zKxzQ1Wca-Lit8OjWCb9CG2u8qYu25B__2DhuPrkaZo-8Z2tIpilI8tNI7o7eW9mqdbXy12HY4rpgu0t7AmECxYOQ5dxngXDgYF47cpSKgZdsmYUI3wrK5CWV7GwBIENvxRPLxMbfMx5CETddLBV4W_D5sNC9dQIwZxHcq1X4YrPB8lKqU7IxfFAXBcxhIkscqneI_VwtlOtY_k-4aLauq__-nodB7vHBdf82RT1wQfsVkbYdvROe46vBJTtgs_JE_uBrswvzZ5v1dCDYEqL7128Wq9zM_BXp96vqwkaj1C62lwDbqOrjQ6RzREBEy2i9MMeyeF2KW8onZmRx9AIx3MP214sGYnSLaBkT_pNKOfHPqTkH0b-R6PSeblQSK92qcatEyiX2Snx_HD9WzyN2TIxDCF3I9KNeRmtmy_TU37rLPs26yLTFEJDNOFYKEqgJCYO6APox8RwxoY9RiJyyFGF4").buildAsync(URI.create("ws://localhost:8080/lxgaming-core/v1/ws"), new WebSocket.Listener() {
                    List<CharSequence> parts = new ArrayList<>();
                    CompletableFuture<?> accumulatedMessage = new CompletableFuture<>();

                    @Override
                    public CompletionStage<?> onText(WebSocket webSocket, CharSequence data, boolean last) {
                        parts.add(data);
                        webSocket.request(1);
                        if (last) {
                            processWholeText(parts);
                            parts = new ArrayList<>();
                            accumulatedMessage.complete(null);
                            CompletionStage<?> cf = accumulatedMessage;
                            accumulatedMessage = new CompletableFuture<>();
                            return cf;
                        }
                        return accumulatedMessage;
                    }
                });

        wsFut.thenCompose(ws -> ws.sendText("{\n" +
                "    \"$type\": \"CreateBettingRequest\",\n" +
                "    \"session_id\": \"2\",\n" +
                "    \"room_address\": \"Oanhdko1223\",\n" +
                "    \"card_id\": \"1\",\n" +
                "    \"mode\":  \"DONGMAT\",\n" +
                "    \"ruby\": 23\n" +
                "}", true))
                .whenComplete((ws, e) -> {
            if (e != null) {
                e.printStackTrace();
            }
        }).join();

    }

    private static void processWholeText(List<CharSequence> parts) {
        System.out.println(parts.stream().collect(Collectors.joining()));
    }
}
